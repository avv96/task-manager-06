package ru.tsc.vinokurov.tm;

import ru.tsc.vinokurov.tm.constant.ArgumentConst;
import ru.tsc.vinokurov.tm.constant.TerminalConst;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public final class Application {

    private static final String TASK_MANAGER_VERSION = "1.6.0";

    private static void displayHelp() {
        System.out.printf("%s, %s - Display program version.\n", TerminalConst.CMD_VERSION, ArgumentConst.VERSION);
        System.out.printf("%s, %s - Display developer info.\n", TerminalConst.CMD_ABOUT, ArgumentConst.ABOUT);
        System.out.printf("%s, %s - Display list of terminal commands.\n", TerminalConst.CMD_HELP, ArgumentConst.HELP);
        System.out.printf("%s - Close Application.\n", TerminalConst.EXIT);
    }

    private static void displayAbout() {
        System.out.println("Developer: Aleksey Vinokurov");
        System.out.println("Email: avv96@yandex.ru");
    }

    private static void displayVersion() {
        System.out.printf("Version: %s\n", TASK_MANAGER_VERSION);
    }

    private static void displayErrorArgument(String arg) {
        System.err.printf("Error! %s is not valid argument!\n", arg);
    }

    private static void displayErrorCommand(String arg) {
        System.err.printf("Error! %s is not valid command!\n", arg);
    }

    private static void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    private static void close() {
        System.exit(0);
    }

    private static void processCommand(final String command) {
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case TerminalConst.CMD_HELP:
                displayHelp();
                break;
            case TerminalConst.CMD_VERSION:
                displayVersion();
                break;
            case TerminalConst.CMD_ABOUT:
                displayAbout();
                break;
            case TerminalConst.EXIT:
                close();
                break;
            default:
                displayErrorCommand(command);
        }
    }

    private static void processCommand() {
        try (final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            String command;
            System.out.print("Enter command: ");
            while ((command = reader.readLine()) != null) {
                processCommand(command);
                System.out.print("Enter command: ");
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void processArgument(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case ArgumentConst.HELP:
                displayHelp();
                break;
            case ArgumentConst.VERSION:
                displayVersion();
                break;
            case ArgumentConst.ABOUT:
                displayAbout();
                break;
            default:
                displayErrorArgument(arg);
        }
    }

    private static boolean processArgument(final String[] args) {
        if (args == null || args.length < 1) return false;
        for (String arg: args) {
            processArgument(arg);
        }
        return true;
    }

    public static void main(String[] args) {
        if (processArgument(args)) close();
        displayWelcome();
        processCommand();
    }

}
